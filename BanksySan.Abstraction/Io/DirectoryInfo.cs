﻿namespace BanksySan.Abstraction.Io
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using RealFileInfo = System.IO.FileInfo;
    using RealDirectoryInfo = System.IO.DirectoryInfo;

    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class DirectoryInfo : FileSystemInfo, IDirectoryInfo
    {
        private readonly RealDirectoryInfo _directoryInfo;

        public DirectoryInfo(RealDirectoryInfo directoryInfo) : base(directoryInfo)
        {
            _directoryInfo = directoryInfo;
        }

        public DirectoryInfo(string path) : this(new RealDirectoryInfo(path))
        {
        }

        public IDirectoryInfo Parent => new DirectoryInfo(_directoryInfo);
        public IDirectoryInfo Root => new DirectoryInfo(_directoryInfo.Root);

        public void Create()
        {
            _directoryInfo.Create();
        }

        public IDirectoryInfo CreateSubdirectory(string path)
        {
            var newDirectory = _directoryInfo.CreateSubdirectory(path);
            return new DirectoryInfo(newDirectory);
        }

        public void Delete(bool recursive)
        {
            _directoryInfo.Delete(recursive);
        }

        public IEnumerable<IDirectoryInfo> EnumerateDirectories()
        {
            return _directoryInfo.EnumerateDirectories().Select(x => new DirectoryInfo(x));
        }

        public IEnumerable<IDirectoryInfo> EnumerateDirectories(string searchPattern)
        {
            return _directoryInfo.EnumerateDirectories(searchPattern).Select(x => new DirectoryInfo(x));
        }

        public IEnumerable<IDirectoryInfo> EnumerateDirectories(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.EnumerateDirectories(searchPattern, searchOption).Select(x => new DirectoryInfo(x));
        }

        public IEnumerable<IFileInfo> EnumerateFiles()
        {
            return _directoryInfo.EnumerateFiles().Select(x => new FileInfo(x));
        }

        public IEnumerable<IFileInfo> EnumerateFiles(string searchPattern)
        {
            return _directoryInfo.EnumerateFiles(searchPattern).Select(x => new FileInfo(x));
        }

        public IEnumerable<IFileInfo> EnumerateFiles(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.EnumerateFiles(searchPattern, searchOption).Select(x => new FileInfo(x));
        }

        public IEnumerable<IFileSystemInfo> EnumerateIFileSystemInfos()
        {
            return _directoryInfo.EnumerateFileSystemInfos().Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            });
        }

        public IEnumerable<IFileSystemInfo> EnumerateIFileSystemInfos(string searchPattern)
        {
            return _directoryInfo.EnumerateFileSystemInfos(searchPattern).Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            });
        }

        public IEnumerable<IFileSystemInfo> EnumerateIFileSystemInfos(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.EnumerateFileSystemInfos(searchPattern, searchOption).Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            });
        }

        public IDirectoryInfo[] GetDirectories()
        {
            return _directoryInfo.GetDirectories().Select(x => (IDirectoryInfo) new DirectoryInfo(x)).ToArray();
        }

        public IDirectoryInfo[] GetDirectories(string searchPattern)
        {
            return _directoryInfo.GetDirectories(searchPattern).Select(x => (IDirectoryInfo) new DirectoryInfo(x))
                .ToArray();
        }

        public IDirectoryInfo[] GetDirectories(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.GetDirectories(searchPattern, searchOption)
                .Select(x => (IDirectoryInfo) new DirectoryInfo(x)).ToArray();
        }

        public IFileInfo[] GetFiles()
        {
            return _directoryInfo.GetFiles().Select(x => (IFileInfo) new FileInfo(x)).ToArray();
        }

        public IFileInfo[] GetFiles(string searchPattern)
        {
            return _directoryInfo.GetFiles(searchPattern).Select(x => (IFileInfo) new FileInfo(x)).ToArray();
        }

        public IFileInfo[] GetFiles(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.GetFiles(searchPattern, searchOption).Select(x => (IFileInfo) new FileInfo(x))
                .ToArray();
        }

        public IFileSystemInfo[] GetFileSystemInfos()
        {
            return _directoryInfo.GetFileSystemInfos().Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            }).ToArray();
        }

        public IFileSystemInfo[] GetFileSystemInfos(string searchPattern)
        {
            return _directoryInfo.GetFileSystemInfos(searchPattern).Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            }).ToArray();
        }

        public IFileSystemInfo[] GetFileSystemInfos(string searchPattern, SearchOption searchOption)
        {
            return _directoryInfo.GetFileSystemInfos(searchPattern, searchOption).Select(x =>
            {
                IFileSystemInfo fsi;

                switch (x)
                {
                    case RealFileInfo file:
                        fsi = new FileInfo(file);
                        break;
                    case RealDirectoryInfo directory:
                        fsi = new DirectoryInfo(directory);
                        break;
                    default:
                        throw new InvalidOperationException($"Cannot convert {x} to a FileSystemObject.");
                }
                return fsi;
            }).ToArray();
        }

        public void MoveTo(string destDirName)
        {
            _directoryInfo.MoveTo(destDirName);
        }
    }
}