﻿namespace BanksySan.Abstraction.Io
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;

    public abstract class FileSystemInfo : IFileSystemInfo
    {
        private readonly System.IO.FileSystemInfo _fileSystemInfo;

        protected FileSystemInfo(System.IO.FileSystemInfo fileSystemInfo)
        {
            _fileSystemInfo = fileSystemInfo;
        }

        public FileAttributes Attributes => _fileSystemInfo.Attributes;
        public DateTime CreationTime => _fileSystemInfo.CreationTime;
        public DateTime CreationTimeUtc => _fileSystemInfo.CreationTimeUtc;
        public bool Exists => _fileSystemInfo.Exists;
        public string Extension => _fileSystemInfo.Extension;
        public string FullName => _fileSystemInfo.FullName;
        public DateTime LastAccessTime => _fileSystemInfo.LastAccessTime;
        public DateTime LastAccessTimeUtc => _fileSystemInfo.LastAccessTimeUtc;
        public DateTime LastWriteTime => _fileSystemInfo.LastWriteTime;
        public DateTime LastWriteTimeUtc => _fileSystemInfo.LastWriteTimeUtc;
        public string Name => _fileSystemInfo.Name;

        public void Delete()
        {
            _fileSystemInfo.Delete();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            _fileSystemInfo.GetObjectData(info, context);
        }

        public void Refresh()
        {
            _fileSystemInfo.Refresh();
        }
    }
}