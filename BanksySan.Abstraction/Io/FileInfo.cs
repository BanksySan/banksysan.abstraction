﻿namespace BanksySan.Abstraction.Io
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using RealFileInfo = System.IO.FileInfo;

    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class FileInfo : FileSystemInfo, IFileInfo
    {
        private readonly RealFileInfo _fileInfo;

        public FileInfo(RealFileInfo fileInfo) : base(fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public FileInfo(string path) : this(new RealFileInfo(path))
        {
        }

        public IDirectoryInfo Directory => new DirectoryInfo(_fileInfo.Directory);
        public string DirectoryName => _fileInfo.DirectoryName;
        public bool IsReadOnly => _fileInfo.IsReadOnly;
        public long Length => _fileInfo.Length;

        public StreamWriter AppendText()
        {
            return _fileInfo.AppendText();
        }

        public IFileInfo CopyTo(string destFileName)
        {
            return new FileInfo(_fileInfo.CopyTo(destFileName));
        }

        public IFileInfo CopyTo(string destFileName, bool overwrite)
        {
            return new FileInfo(_fileInfo.CopyTo(destFileName, overwrite));
        }

        public FileStream Create()
        {
            return _fileInfo.Create();
        }

        public StreamWriter CreateText()
        {
            return _fileInfo.CreateText();
        }

        public void Decrypt()
        {
            _fileInfo.Decrypt();
        }

        public void Encrypt()
        {
            _fileInfo.Encrypt();
        }

        public void MoveTo(string destFileName)
        {
            _fileInfo.MoveTo(destFileName);
        }

        public FileStream Open(FileMode mode)
        {
            return _fileInfo.Open(mode);
        }

        public FileStream Open(FileMode mode, FileAccess access)
        {
            return _fileInfo.Open(mode, access);
        }

        public FileStream Open(FileMode mode, FileAccess access, FileShare share)
        {
            return _fileInfo.Open(mode, access, share);
        }

        public FileStream OpenRead()
        {
            return _fileInfo.OpenRead();
        }

        public StreamReader OpenText()
        {
            return _fileInfo.OpenText();
        }

        public FileStream OpenWrite()
        {
            return _fileInfo.OpenWrite();
        }

        public IFileInfo Replace(string destinationFileName, string destinationBackupFileName)
        {
            var newFileInfo = _fileInfo.Replace(destinationFileName, destinationBackupFileName);
            return new FileInfo(newFileInfo);
        }

        public IFileInfo Replace(string destinationFileName, string destinationBackupFileName,
            bool ignoreMetadataErrors)
        {
            var newFileInfo = _fileInfo.Replace(destinationFileName, destinationBackupFileName, ignoreMetadataErrors);
            return new FileInfo(newFileInfo);
        }
    }
}