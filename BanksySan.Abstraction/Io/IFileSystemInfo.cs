﻿namespace BanksySan.Abstraction.Io
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;

    public interface IFileSystemInfo
    {
        FileAttributes Attributes { get; }
        DateTime CreationTime { get; }
        DateTime CreationTimeUtc { get; }
        bool Exists { get; }
        string Extension { get; }
        string FullName { get; }
        DateTime LastAccessTime { get; }
        DateTime LastAccessTimeUtc { get; }
        DateTime LastWriteTime { get; }
        DateTime LastWriteTimeUtc { get; }
        string Name { get; }
        void Delete();
        void GetObjectData(SerializationInfo info, StreamingContext context);
        void Refresh();
    }
}