﻿namespace BanksySan.Abstraction.Tests.Helpers
{
    internal static class FilePathsHelper
    {
        public static string InvalidFilePath => @"this-is-an-invalid-path\/?*";
    }
}