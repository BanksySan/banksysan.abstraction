﻿namespace BanksySan.Abstraction.Tests.Io
{
    using System;
    using System.IO;
    using System.Linq;
    using Xunit;
    using DirectoryInfo = Abstraction.Io.DirectoryInfo;
    using RealDirectoryInfo = System.IO.DirectoryInfo;

    public class DirectoryInfoTests
    {
        [Fact]
        public void GetFileSystemInfos()
        {
            var filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            var realDirectoryInfo = new RealDirectoryInfo(filePath);
            var directoryInfo = new DirectoryInfo(filePath);

            var realFileSystemObjects = realDirectoryInfo.GetFileSystemInfos();
            var fileSystemObjects = directoryInfo.GetFileSystemInfos();

            Assert.Equal(realFileSystemObjects.Select(x => x.FullName), fileSystemObjects.Select(x => x.FullName));
            Assert.Equal(realFileSystemObjects.Select(x => x.GetType().Name),
                fileSystemObjects.Select(x => x.GetType().Name));
        }

        [Fact]
        public void MoveDirectory()
        {
            var originalPath = Path.GetTempPath() + Path.GetRandomFileName() + Path.DirectorySeparatorChar;
            var destinationPath = Path.GetTempPath() + Path.GetRandomFileName() + Path.DirectorySeparatorChar;
            var realDirectoryInfo = new RealDirectoryInfo(originalPath);

            var directoryInfo = new DirectoryInfo(realDirectoryInfo);

            Assert.False(directoryInfo.Exists);
            directoryInfo.Create();
            directoryInfo.Refresh();
            Assert.True(directoryInfo.Exists);
            directoryInfo.MoveTo(destinationPath);
            directoryInfo.Refresh();
            Assert.Equal(directoryInfo.FullName, destinationPath);
        }
    }
}