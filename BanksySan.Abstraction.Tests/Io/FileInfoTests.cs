namespace BanksySan.Abstraction.Tests.Io
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Helpers;
    using Xunit;
    using Xunit.Abstractions;
    using FileInfo = Abstraction.Io.FileInfo;
    using RealFileInfo = System.IO.FileInfo;

    public class FileInfoTests
    {
        public FileInfoTests(ITestOutputHelper output)
        {
            _output = output;
        }

        private readonly ITestOutputHelper _output;
        private readonly string _tempDirectoryPath = Path.GetTempPath();

        [Theory]
        [ClassData(typeof(FilePathsData))]
        public void ConstructionWorksWithFileInfo(string filePath)
        {
            var realFileInfo = new RealFileInfo(filePath);
            var fileInfo = new FileInfo(realFileInfo);

            Assert.Equal(fileInfo.FullName, realFileInfo.FullName);
            Assert.Equal(fileInfo.Directory.FullName, realFileInfo.Directory.FullName);
            Assert.Equal(fileInfo.Directory.GetFileSystemInfos().Select(x => x.FullName),
                realFileInfo.Directory.GetFileSystemInfos().Select(x => x.FullName));
        }

        [Theory]
        [ClassData(typeof(FilePathsData))]
        public void ConstructionWorksWithFilePath(string filePath)
        {
            var realFileInfo = new FileInfo(filePath);
            var fileInfo = new FileInfo(filePath);

            Assert.Equal(fileInfo.FullName, realFileInfo.FullName);
            Assert.Equal(fileInfo.Directory.FullName, realFileInfo.Directory.FullName);
            Assert.Equal(fileInfo.Directory.GetFileSystemInfos().Select(x => x.FullName),
                realFileInfo.Directory.GetFileSystemInfos().Select(x => x.FullName));
        }

        public class FilePathsData : IEnumerable<object[]>
        {
            private readonly IEnumerable<object[]> _filePaths;

            public FilePathsData()
            {
                ExistentFilePath = Path.GetTempFileName();
                NonExistentFilePath = Path.GetTempFileName();
                _filePaths = new[]
                {
                    new[] {ExistentFilePath},
                    new[] {NonExistentFilePath}
                };
                File.Create(ExistentFilePath);
            }

            public string NonExistentFilePath { get; }

            public string ExistentFilePath { get; }

            public IEnumerator<object[]> GetEnumerator()
            {
                return _filePaths.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        [Fact]
        public void CopyFile()
        {
            var originalFilePath = Path.GetTempFileName();
            var destinationFilePath = _tempDirectoryPath + Path.GetRandomFileName();

            _output.WriteLine($"   Original file: {originalFilePath}");
            _output.WriteLine($"Destination file: {destinationFilePath}");

            var realFileInfo = new RealFileInfo(originalFilePath);
            var fileInfo = new FileInfo(realFileInfo);

            var newFile = fileInfo.CopyTo(destinationFilePath);
            Assert.Equal(originalFilePath, fileInfo.FullName);
            Assert.Equal(destinationFilePath, newFile.FullName);
        }

        [Fact]
        public void InvalidFilePath()
        {
            Assert.Throws<ArgumentException>(() => new FileInfo(FilePathsHelper.InvalidFilePath));
        }

        [Fact]
        public void MoveFile()
        {
            var originalFilePath = Path.GetTempFileName();
            var destinationFilePath = _tempDirectoryPath + Path.GetRandomFileName();

            _output.WriteLine($"   Original file: {originalFilePath}");
            _output.WriteLine($"Destination file: {destinationFilePath}");

            var realFileInfo = new RealFileInfo(originalFilePath);
            var fileInfo = new FileInfo(realFileInfo);

            fileInfo.MoveTo(destinationFilePath);
            Assert.Equal(destinationFilePath, fileInfo.FullName);
        }
    }
}